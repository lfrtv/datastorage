require_relative 'saving_articles'
desc "This task is called by the Heroku scheduler add-on"
task :update_articles => :environment do
  puts "Updating articles..."
  save_articles()
  puts "Done!"
end

task :clear_articles => :environment do
  puts "Clear articles..."
  Article.delete_all
  puts "Done!"
end