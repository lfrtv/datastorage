require 'rest-client'
require 'indico'
require 'rails/configuration'
def save_articles
  nyt_url = "https://api.nytimes.com/svc/topstories/v2/home.json?&api-key="
  data = JSON.parse( RestClient.get("#{nyt_url}#{'bf73c59b29444cb28ceeda2b0b73bd61'}") )
  data["results"].each do |article, index|
    existing_article = Article.find_by(title: article["title"])
    if article["abstract"].length > 0
      if !existing_article
        config = {api_key: "#{ENV["INDICO_API_KEY"]}"}
        news = Article.new do |key|
          key.title = article["title"]
          key.abstract = article["abstract"]
          key.url = article["url"]
          key.date = Time.now
        end
        if news.save
          puts "Article #{article["title"]} saved"
        end
      end
    end
  end
end