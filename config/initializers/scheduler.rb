require 'rufus-scheduler'
require './lib/tasks/saving_articles'

# Let's use the rufus-scheduler singleton
#
s = Rufus::Scheduler.singleton


# Stupid recurrent task...
#
s.every '1m' do
  Rails.logger.info "Update articles...."
  save_articles()
  Rails.logger.info "Done!"
end