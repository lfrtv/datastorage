module Types
  class QueryType < Types::BaseObject
    # Add root-level fields here.
    # They will be entry points for queries on your schema.

    field :article, ArticleType, null: true do
      description "Find a article by ID"
      argument :id, ID, required: true
    end

    def article(id:)
      Article.find(id)
    end

    field :article_by_title, [ArticleType], null: true do
      description "Find a article by title"
      argument :title, String, required: true
    end

    def article_by_title(title:)
      puts title
      Article.where("title LIKE '%#{title}%'")
    end

    field :articles, [ArticleType], null: true do
      description "Find all articles"
      argument :title, String, required: false
      argument :abstract, String, required: false
      argument :url, String, required: false
    end

    def articles(**args)
      filter_params = {
        :title_filter => args[:title], 
        :abstract_filter => args[:abstract],
        :url_filter => args[:url],
      }
      Article.filter(filter_params)
    end

  end
end
