json.extract! article, :id, :title, :abstract, :url, :date, :created_at, :updated_at
json.url article_url(article, format: :json)
