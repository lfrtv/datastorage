class ArticlesController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :set_article, only: [:show, :edit, :update, :destroy]
  swagger_controller :article, "Article"

  swagger_api :index do
    summary "Fetches all Article items"
    param :query, :title, :string, :optional, "Title contain"
    response :unauthorized
    response :not_acceptable
  end

  swagger_api :show do
    summary "Fetches a single User item"
    param :path, :id, :integer, :optional, "User Id"
    response :ok, "Success", :User
    response :unauthorized
    response :not_acceptable
    response :not_found
  end

  swagger_api :create do
    summary "Creates a new Article"
    param :form, 'article[title]', :string, :required, "Title"
    param :form, 'article[abstract]', :string, :required, "Annotation"
    param :form, 'article[url]', :string, :required, "Url"
    response :unauthorized
    response :not_acceptable
  end

  swagger_api :update do
    summary "Updates an existing Article"
    param :path, :id, :integer, :required, "Article Id"
    param :form, 'article[title]', :string, :optional, "Title"
    param :form, 'article[abstract]', :string, :optional, "Annotation"
    param :form, 'article[url]', :string, :optional, "Url"
    response :unauthorized
    response :not_found
    response :not_acceptable
  end

  swagger_api :destroy do
    summary "Deletes an existing Article item"
    param :path, :id, :integer, :required, "Article Id"
    response :unauthorized
    response :not_found
  end

  # GET /articles
  # GET /articles.json
  def index
    @articles = Article.all
    if params[:title]
      @articles = Article.where("title LIKE '%#{params[:title]}%'")
    end
  end

  # GET /articles/1
  # GET /articles/1.json
  def show
  end

  # GET /articles/new
  def new
    @article = Article.new
  end

  # GET /articles/1/edit
  def edit
  end

  # POST /articles
  # POST /articles.json
  def create
    @article = Article.new(article_params)

    respond_to do |format|
      if @article.save
        format.html { redirect_to @article, notice: 'Article was successfully created.' }
        format.json { render :show, status: :created, location: @article }
      else
        format.html { render :new }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /articles/1
  # PATCH/PUT /articles/1.json
  def update
    respond_to do |format|
      if @article.update(article_params)
        format.html { redirect_to @article, notice: 'Article was successfully updated.' }
        format.json { render :show, status: :ok, location: @article }
      else
        format.html { render :edit }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /articles/1
  # DELETE /articles/1.json
  def destroy
    @article.destroy
    respond_to do |format|
      format.html { redirect_to articles_url, notice: 'Article was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_article
      @article = Article.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def article_params
      params.require(:article).permit(:title, :abstract, :url, :date)
    end
end
